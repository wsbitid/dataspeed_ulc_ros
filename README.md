# Dataspeed Universal Lat/Lon Controller Interface
This repository contains a ROS interface for the Universal Lat/Lon Controller (ULC) feature in Dataspeed ADAS Kit firmware.

The User's Guide for the ULC can be found on the [downloads page](https://bitbucket.org/DataspeedInc/dataspeed_ulc_ros/downloads) 